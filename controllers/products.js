"use strict";

const debug = require('debug')("restful:controllers:products");
const ObjectId = require('mongodb').ObjectID;

function Products(main) {
    debug('init...');
    return {
        'search': (req, res, next) => {
            debug('.search called');
            let name = req.swagger.params.name.value ? req.swagger.params.name.value.toLowerCase() : null;
            let id = req.swagger.params.id.value ? req.swagger.params.id.value.toLowerCase() : null;
            let query = {};
            if (name) {
                query.name = { $regex: name, $options: 'i' };
            }
            if (id) {
                query._id = ObjectId(id);
            }
            main.libs.products.find(query)
                .then((products) => {
                    res.json({products});
                })
                .catch(next);
        },
        'insert': (req, res, next) => {
            debug('.insert called');
            main.libs.products.add(req.swagger.params.product.value)
                .then((resp) => {
                    res.json({ status: 'OK' });
                })
                .catch(next);
        },
        'delete': (req, res, next) => {
            debug('.delete called');
            main.libs.products.delete({ _id: ObjectId(req.swagger.params._id.value) })
                .then((resp) => {
                    res.json({ status: 'OK' });
                })
                .catch(next);
        }
    }
}

module.exports = Products;