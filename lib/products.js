"use strict";

function Products(main) {
    this.db = main.db;
}

Products.prototype.add = function (params) {
    let self = this;
    return new Promise((resolve, reject) => {
        self.db.Products.insert(params, (err, docs) => {
            err ? reject(err) : resolve(docs);
        });
    });
};

Products.prototype.find = function (query) {
    let self = this;

    return new Promise((resolve, reject) => {
        self.db.Products.find(query).limit(10, (err, docs) => {
            err ? reject(err) : resolve(docs);
        });
    });
};

Products.prototype.delete = function (query) {
    let self = this;

    return new Promise((resolve, reject) => {
        self.db.Products.remove(query, (err, docs) => {
            err ? reject(err) : resolve(docs);
        });
    });
};

module.exports = Products;