var zones = ['America', 'Europe', 'South America', 'Centralamerica', 'Caribbean', 'Middle East', 'East Asia', 'Southeast Asia', 'Africa', 'Oceania'];
var names = ['Basico', 'Intermedio', 'Premium', 'Discount'];

var getRandom = function (array) {
	return array[Math.floor(Math.random() * array.length - 1)];
};

var getRandomInt = function (seed, max) {
	var value = -1;
	if (max) {
		do {
			value = Math.floor(Math.random() * seed)
		} while (value < max)
		return value;
	}
	return Math.floor(Math.random() * seed);
};

var getZone = function (alreadyPicked) {
    var zone = '';
    do {
        zone = zones[Math.floor(Math.random() * zones.length - 1)];
    } while (alreadyPicked.indexOf(zone) > -1);
    return zone;
};

var getZones = function () {
    var zones = [];
    var limit = Math.floor(Math.random() * 5);
    for (var i = 0; i < limit; i++) {
        zones.push(getZone(zones));
    }
    return zones;
};

var getProduct = function () {
	var product = {
		name: getRandom(names) + ' ' + Math.floor(Math.random() * 100000),
		costMin: getRandomInt(500000),
		additional: getRandomInt(10000),
		adults: true,
		badge: getRandomInt(10000000),
		zones: getZones()
	}	
	product.min = getRandomInt(120);
	product.max = getRandomInt(360, product.min);
	product.days = [];
	for (var i = product.min; i < product.max; i++) {
		product.days.push({ day: i, cost: product.costMin + (i - product.min) * product.additional });
	}
	return product;
}

for (var i = 0; i < 10000; i++) {
	db.products.insert(getProduct());
};